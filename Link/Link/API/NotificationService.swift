//
//  NotificationService.swift
//  Link
//
//  Created by none on 17/1/21.
//

import Firebase

struct NotificationService {
    
    /// when i go to like the Guy post, I need to upload that notification to the Guy's list of notifications
    /// so the Guy open his App and goes through the notification, he sees that he has a notification from me
    /// optional because for the following action - notification
    static func uploadNotification(toUid uid: String, fromUser: User, type: NotificationType, post: Post? = nil) {
        
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        // if I like my own post, it would be to the post owner -> will not show notification
        guard uid != currentUid else { return }
        
        let docRef = COLLECTION_NOTIFICATIONS.document(uid).collection("user-notifications").document()
        
        var data: [String: Any] = ["timestamp": Timestamp(date: Date()),
                                   "uid": fromUser.uid,
                                   "type": type.rawValue,
                                   "id": docRef.documentID,
                                   "userProfileImageUrl": fromUser.profileImageUrl,
                                   "username": fromUser.username]
        
        if let post = post {
            data["postId"] = post.postId
            data["postImageUrl"] = post.imageUrl
        }
        
        docRef.setData(data)
        
    }
    
    static func fetchNotifications(completion: @escaping([Notification]) -> Void) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let query = COLLECTION_NOTIFICATIONS.document(uid).collection("user-notifications").order(by: "timestamp", descending: true)
        
        query.getDocuments { snapshot, _ in
            guard let documents = snapshot?.documents else { return }
            
            /// all we need for the notification in our initial there is the dictionary (Notification - Model)
            /// looking at all documents in "user-notification" and map use a mapping function to get return data into a notification
            /// $0.data -> each one data in user-notification
            ///
            let notifications = documents.map({ Notification(dictionary: $0.data()) })
            /// execute our completion handler with the notifications that we get back
            completion(notifications)
        }
        
    }
    
}
