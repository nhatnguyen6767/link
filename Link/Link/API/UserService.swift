//
//  UserService.swift
//  Link
//
//  Created by none on 9/1/21.
//

import UIKit
import Firebase

/// this is the completion handler for Firestore API calls
/// custom this so don't have to type this every time we want to execute that completion handler
///
typealias FirestoreCompletion = (Error?) -> Void

struct UserService {
    /// won't execute until this whole process is completed
    /// when call it back in the profileController, all of complete block completed, want to ask it to give back user value
    static func fetchUser(withUid uid: String, completion: @escaping(User) -> Void) {
        COLLECTION_USERS.document(uid).getDocument { snapshot, error in
            
            guard let dictionary = snapshot?.data() else { return }
            let user = User(dictionary: dictionary)
            completion(user)
            
        }
    }
    
    static func fetchUsers(completion: @escaping([User]) -> Void) {
        //        var users = [User]()
        COLLECTION_USERS.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else { return }
            ///            *same meaning*
            ///            snapshot.documents.forEach { document in
            ///                print("DEBUG: Document in service file \(document.data())")
            ///                let user = User(dictionary: document.data())
            ///                users.append(user)
            ///            }
            ///            completion(users)
            
            /// mapping function basically takes one data object and then perform some sort of mapping and convert it into another data object
            /// documents array on the from the snapshot, all we need from that documents array is the document data for each particular document
            /// so we're saying that we want to map the documents array into an array of users
            /// and in the documents. it's going to get the data that we need and just map each thing over to a user object and put all of that inside array
            /// map the documents into an user object and going to ask us for a dictionary
            /// $0 -> just represents each one of the document in that document array
            /// $0.data() -> each one of the documents data in document array then populate this user's array
            ///
            let users = snapshot.documents.map({ User(dictionary: $0.data()) })
            completion(users)
        }
        
    }
    
    static func follow(uid: String, completion: @escaping(FirestoreCompletion)) {
        
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        /// in the following (firestore) have current user (uid) -> user-following (collection) -> user id that they just followed
        /// the first [:] is mean we dont need any information in that, we already have all this user's information inside of the user structure
        ///
        
        COLLECTION_FOLLOWING.document(currentUid).collection("user-following").document(uid).setData([:]) { error in
            
            /// do the same thing with followers
            /// ex: when the FIRST GUY just followed the SECOND GUY, so we will add SECOND GUY to the list of people that
            ///  FIRST GUY follows. And we need to add FIRST GUY to the list of people that follow SECOND GUY
            ///
            
            COLLECTION_FOLLOWERS.document(uid).collection("user-followers").document(currentUid).setData([:], completion: completion)
            
        }
    }
    
    static func unfollow(uid: String, completion: @escaping(FirestoreCompletion)) {
        
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        
        COLLECTION_FOLLOWING.document(currentUid).collection("user-following").document(uid).delete { error in
            
            COLLECTION_FOLLOWERS.document(uid).collection("user-followers").document(currentUid).delete(completion: completion)
            
        }
        
    }
    
    static func checkIfUserIsFollow(uid: String, completion: @escaping(Bool) -> Void) {
        /// current users following structure (following in firestore), check and see if the user's profile that we're looking at,
        /// if this list contains the document (uid). If does, then say following this user (uid in the user-following)
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        COLLECTION_FOLLOWING.document(currentUid).collection("user-following").document(uid).getDocument { (snapshot, error) in
            guard let isFollowed = snapshot?.exists else { return }
            completion(isFollowed)
        }
        
    }
    
    static func fetchUserStats(uid: String, completion: @escaping(UserStats) -> Void) {
        COLLECTION_FOLLOWERS.document(uid).collection("user-followers").getDocuments { (snapshot, _) in
            let followers = snapshot?.documents.count ?? 0
            
            COLLECTION_FOLLOWING.document(uid).collection("user-following").getDocuments { (snapshot, _) in
                let following = snapshot?.documents.count ?? 0
                
                COLLECTION_POSTS.whereField("ownerUid", isEqualTo: uid).getDocuments { (snapshot, _) in
                    let posts = snapshot?.documents.count ?? 0
                    completion(UserStats(followers: followers, following: following, posts: posts))
                }
                
            }
        }
    }
}
