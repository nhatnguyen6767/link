//
//  PostService.swift
//  Link
//
//  Created by none on 11/1/21.
//

import UIKit
import Firebase

struct PostService {
    
    static func uploadPost(caption: String, image: UIImage, user: User, completion: @escaping(FirestoreCompletion)) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        // we have ImageUploader function when register user
        ImageUploader.uploadImage(image: image) { imageUrl in
            let data = ["caption": caption,
                        "timestamp": Timestamp(date: Date()),
                        "likes": 0,
                        "imageUrl": imageUrl,
                        "ownerUid": uid,
                        "ownerImageUrl": user.profileImageUrl,
                        "ownerUsername": user.username] as [String : Any]
            
            let docRef = COLLECTION_POSTS.addDocument(data: data, completion: completion)
            
            self.updateUserFeedAfterPost(postId: docRef.documentID)
        }
    }
    
    // fetch post from database
    
    static func fetchPosts(completion: @escaping([Post]) -> Void) {
        // sort - go from the newest post to the oldest post
        COLLECTION_POSTS.order(by: "timestamp", descending: true).getDocuments { (snapshot, error) in
            
            guard let documents = snapshot?.documents else { return }
            
//            documents.forEach { doc in
//                print("DEBUG: Doc data is \(doc.data())")
//            }
//            // - explain: go to the fetchUser in UserService
            
            let posts = documents.map({ Post(postId: $0.documentID, dictionary: $0.data()) })
            completion(posts)
            
        }
    }
    
    static func fetchPosts(forUser uid: String, completion: @escaping([Post]) -> Void) {
        /// this is unique to the capabilities we get from firestore
        /// can query down by timestamp, some field path is equal to something
        ///
        /// go to the posts in firestore -> that I want to go into the post and get me all the posts
        /// where the uid (ownerUid) is equal to the Uid that we pass into this function
        /// Ex: I pass in the Join UID and it's only going to give me back the posts that are associated with Join UID
        ///
        /// just sort manual -> can't find where a field is equal to something and then order it by something eles
        let query = COLLECTION_POSTS.whereField("ownerUid", isEqualTo: uid)
        
        query.getDocuments { (snapshot, error) in
            
            guard let documents = snapshot?.documents else { return }
            
            var posts = documents.map({ Post(postId: $0.documentID, dictionary: $0.data()) })
            
            posts.sort(by: { $0.timestamp.seconds > $1.timestamp.seconds })
            
//            posts.sort { (post1, post2) -> Bool in
//                return post1.timestamp.seconds > post2.timestamp.seconds
//            }
            
            completion(posts)
            
        }
    }
    
    static func fetchPost(withPostId postId: String, completion: @escaping(Post) -> Void) {
        
        COLLECTION_POSTS.document(postId).getDocument { snapshot, _ in
            guard let snapshot = snapshot else { return }
            guard let data = snapshot.data() else { return }
            let post = Post(postId: snapshot.documentID, dictionary: data)
            completion(post)
        }
        
    }
    
    /// keeping track of what I have liked and what post contain likes from users
    /// I want to see all the posts that a user has like,
    
    /// *Like action recap - 6*
    static func likePost(post: Post, completion: @escaping(FirestoreCompletion)) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        // update and increa value of likes
        COLLECTION_POSTS.document(post.postId).updateData(["likes": post.likes + 1])
        
        COLLECTION_POSTS.document(post.postId).collection("post-likes").document(uid).setData([:]) { _ in
            
            COLLECTION_USERS.document(uid).collection("user-likes").document(post.postId).setData([:], completion: completion)
        }
        
    }
    
    static func unlikePost(post: Post, completion: @escaping(FirestoreCompletion)) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        /// make sure that the post likes are greater than zero.
        /// Because some reason, something gets out of whack with our database determining or not users like to post
        /// when like is zero and we hit that like button, so something get mixed up with the did like property
        /// and then the post likes went to one. that would throw everything out of whack
        
        /// avoid going into any negative values
        guard post.likes > 0 else { return }
        
        COLLECTION_POSTS.document(post.postId).updateData(["likes": post.likes - 1])
        
        COLLECTION_POSTS.document(post.postId).collection("post-likes").document(uid).delete { _ in
            COLLECTION_USERS.document(uid).collection("user-likes").document(post.postId).delete(completion: completion)
        }
    }
    
    // looking at the collection if have user id -> liked, if don't have ... looking at checkIfUserIsFollow in the UserService
    /// *Like action recap - 7* want to be able to tell what posts a user has liked
    static func checkIfUserLikedPost(post: Post, completion: @escaping(Bool) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        COLLECTION_USERS.document(uid).collection("user-likes").document(post.postId).getDocument { (snapshot, error) in
            
            guard let didLike = snapshot?.exists else { return }
            completion(didLike)
        }
    }
    
    static func fetchFeedPosts(completion: @escaping([Post]) -> Void) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        var posts = [Post]()
        
        COLLECTION_USERS.document(uid).collection("user-feed").getDocuments { snapshot, error in
            snapshot?.documents.forEach({ document in
                fetchPost(withPostId: document.documentID) { post in
                    posts.append(post)
                    
                    posts.sort(by: { $0.timestamp.seconds > $1.timestamp.seconds })
                    
//                    posts.sort { (post1, post2) -> Bool in
//                        return post1.timestamp.seconds > post2.timestamp.seconds
//                    }
                    
                    completion(posts)
                }
            })
        }
        
    }
    
    static func updateUserFeedAfterFollowing(user: User, didFollow: Bool) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let query = COLLECTION_POSTS.whereField("ownerUid", isEqualTo: user.uid)
        
        query.getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else { return }
            
            let docIDs = documents.map({ $0.documentID })
            // add all documentID (postID) into user -> user-feed
            docIDs.forEach { id in
                if didFollow {
                    COLLECTION_USERS.document(uid).collection("user-feed").document(id).setData([:])
                } else {
                    COLLECTION_USERS.document(uid).collection("user-feed").document(id).delete()
                }
                
            }
        }
        
    }
    
    private static func updateUserFeedAfterPost(postId: String) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        /// grabbing the list of all the followers that we have
        COLLECTION_FOLLOWERS.document(uid).collection("user-followers").getDocuments { snapshot, _ in
            guard let documents = snapshot?.documents else { return }
            
            documents.forEach { document in
                COLLECTION_USERS.document(document.documentID).collection("user-feed").document(postId).setData([:])
            }
            
            COLLECTION_USERS.document(uid).collection("user-feed").document(postId).setData([:])
        }
        
    }
    
}
