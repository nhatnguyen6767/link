//
//  CommentService.swift
//  Link
//
//  Created by none on 14/1/21.
//

import Firebase

struct CommentService {
    
    static func uploadComment(comment: String, postID: String, user: User, completion: @escaping(FirestoreCompletion)) {
        
        let data: [String: Any] = ["uid": user.uid, "comment": comment, "timestamp": Timestamp(date: Date()), "username": user.username, "profileImageUrl": user.profileImageUrl]
        
        // comments belong to posts, likes belong to posts (by postID)
        COLLECTION_POSTS.document(postID).collection("comments").addDocument(data: data, completion: completion)
        
    }
    
    /// We have collection View and it's going to be populated with a bunch of comments.
    /// We just created data model, here we want to execute this completion handler and have it give us back an array of comments.
    /// We can populate our collection view with that array of comments
    static func fetchComments(forPost postID: String, completion: @escaping(([Comment]) -> Void)) {
        
        var comments = [Comment]()
        
        let query = COLLECTION_POSTS.document(postID).collection("comments").order(by: "timestamp", descending: true)
        /// whenever something gets added to this comment structure, it listens for those events
        /// and then it will allow us to update our UI based on that event listener
        /// a comment was added to the structure and it will refresh that comment data
        /// because of that listener on the database structure and then we will be able to update our UI with that new comment
        /// as soon as it gets added to the database, rather than having perform a API call manually
        /// Manuall -> if you want new posts, show up, you have to pull down to refresh and manually execute an API call -> realtime
        query.addSnapshotListener { (snapshot, error) in
            snapshot?.documentChanges.forEach({ change in
                // when something added to the database structure
                if change.type == .added {
                    let data = change.document.data()
                    let comment = Comment(dictionary: data)
                    comments.append(comment)
                }
            })
            
            completion(comments)
            
        }
        
    }
    
}
