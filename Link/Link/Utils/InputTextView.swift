//
//  InputTextView.swift
//  Link
//
//  Created by none on 11/1/21.
//

import UIKit

// custom textview (for caption when upload post)
class InputTextView: UITextView {
    
    // MARK: - Properties
    
    /// we're going to set this property whenever we create this class, which is going to be back out of controller
    /// then whenever it gets set, it's going to set this placehoder label's text to whatever
    /// we set that placeholder text
    ///
    var placeholderText: String? {
        didSet {
            placeholderLabel.text = placeholderText
        }
    }
    
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        return label
    }()
    
    var placeholderShouldCenter = true {
        didSet {
            if placeholderShouldCenter {
                placeholderLabel.anchor(left: leftAnchor, right: rightAnchor, paddingLeft: 8)
                placeholderLabel.centerY(inView: self)
            } else {
                placeholderLabel.anchor(top: topAnchor, left: leftAnchor, paddingTop: 6, paddingLeft: 8)
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        addSubview(placeholderLabel)
        placeholderLabel.anchor(top: topAnchor, left: leftAnchor, paddingTop: 6, paddingLeft: 8)
        
        // when typing, placeholder will disappear // observe: Theo dõi
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextDidChange), name: UITextView.textDidChangeNotification, object: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc func handleTextDidChange() {
        // if text is Empty (true) -> dont hide, else -> else
        placeholderLabel.isHidden = !text.isEmpty
    }
    
}
