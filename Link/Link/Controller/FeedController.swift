//
//  FeedController.swift
//  Link
//
//  Created by none on 6/1/21.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

class FeedController: UICollectionViewController {
    
    // MARK: - Properties
    
    // everytime something gets modified in this post, it's going to reload
    /// *Like action recap - 10* anytime set the posts or add post, remove post, or set property on a post that exists in array.
    /// did set block will get called
    private var posts = [Post]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    /// if this post has a value, then we only want to show one post in the feed
    /// and if it doesn't, we want to show all these posts This is happen when tap on a image in the list of image on profile
    var post: Post? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchPosts()
        
        if post != nil {
            checkIfUserLikedPosts()
        }
    }
    
    // MARK: - Actions
    
    @objc func handleRefresh() {
        
        posts.removeAll()
        fetchPosts()
        
    }
    
    @objc func handleLogout() {
        do {
            try Auth.auth().signOut()
            
            let controller = LoginController()
            // *same MainTabbarController of check login *
            controller.delegate = self.tabBarController as? MainTabController
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            
        } catch {
            print("DEBUG: Failed to sign out.")
        }
    }
    
    // MARK: - API
    
    func fetchPosts() {
        /// it's only this post func is only going to actually execute the API call if post is equal to nil
        /// and the post's value -> going to be passing into controller from the profile when we click image
        /// if the post is not equal to nil, it's going to stop at this statement and it won't execute the rest
        guard post == nil else { return }
        //        PostService.fetchPosts { posts in
        //            self.posts = posts
        //
        //            /// *Like action recap - 8* after fetch all the posts
        //            /// it sets our posts property (self.posts = posts) then it checks to see if users like to post
        //            self.checkIfUserLikedPosts()
        //
        //            // must be end refresh ..
        //            self.collectionView.refreshControl?.endRefreshing()
        //
        //        }
        
        PostService.fetchFeedPosts { posts in
            self.posts = posts
            /// *Like action recap - 8* after fetch all the posts
            /// it sets our posts property (self.posts = posts) then it checks to see if users like to post
            self.checkIfUserLikedPosts()
            
            // must be end refresh ..
            self.collectionView.refreshControl?.endRefreshing()
        }
        
    }
    
    /// first - fetch all the posts, check to see if the user has liked the set of posts that we have fetched
    /// as opposed to trying to do it all at once. Everytime we fetch a post, check and see if the user has like that post
    /// and then set that did like property.. little slow
    /// my case. fetch all the posts first and then after you fetch the post, -> go check and see if the user has like those posts
    ///
    
    /// *Like action recap - 9*
    func checkIfUserLikedPosts() {
        
        if let post = post {
            
            /// checking to see if that post is single post
            
            PostService.checkIfUserLikedPost(post: post) { didLike in
                self.post?.didLike = didLike
            }
            
        } else {
            
            posts.forEach { post in
                PostService.checkIfUserLikedPost(post: post) { didLike in
                    /// print("DEBUG: Post is \(post.caption) and user liked is \(didLike)")
                    /// this data's getting back is kind of all out of whack, we've sorted these posts one way and it's just not showing match up
                    /// And we want, we have to go through that post array and find the particular post we're looking at
                    /// each one of the posts -> find the particular post -> get the index
                    /// the meaning : go through post and find me the first index where this post id is equal to the index that we've looking at
                    /// firstIndex -> does its own sort of like looping functionality where it's going to go through the entrie array and look for the particular post that meets this
                    /// $0 -> allow to do some shorthand notation, represents ech one of the posts in that function (firstIndex)
                    /// go through all the posts and find the first index where post i'm looking at is equal to this postID
                    ///
                    if let index = self.posts.firstIndex(where: { $0.postId == post.postId }) {
                        /// after find the index that needed, then update post index (posts array) with the index that we have found -> set true
                        self.posts[index].didLike = didLike
                    }
                    
                    
                }
            }
            
        }
        
        
    }
    
    // MARK: - Helpers
    
    func configureUI() {
        
        collectionView.backgroundColor = .white
        collectionView.register(FeedCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        if post == nil {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        }
        
        navigationItem.title = "Feed"
        
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refresher
        
    }
    
}

// MARK: - UICollectionViewDataSource

extension FeedController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return post == nil ? posts.count : 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FeedCell
        
        cell.delegate = self
        
        if let post = post {
            cell.viewModel = PostViewModel(post: post)
        } else {
            cell.viewModel = PostViewModel(post: posts[indexPath.row])
        }
        return cell
    }
    
}

// MARK: -UICollectionViewDelegateFlowLayout
// to define (size) each cell
extension FeedController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.width
        // image have width = height 8 - space of avatar and post image, 40 - avatar, 8 - space of postImage and like button
        var height = width + 8 + 40 + 8
        // space of like button, comment , label
        height += 50
        // another ..
        height += 60
        return CGSize(width: width, height: height)
    }
}

// MARK: - FeedCellDelegate

extension FeedController: FeedCellDelegate {
    
    func cell(_ cell: FeedCell, wantsToShowCommentsFor post: Post) {
        let controller = CommentController(post: post)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /// *Like action recap - 5* implement in feed controller because that's where we want to delegate the action to.
    func cell(_ cell: FeedCell, didLike post: Post) {
//        post.didLike.toggle()
        
        guard let tab = tabBarController as? MainTabController else { return }
        guard let user = tab.user else { return }
        
        cell.viewModel?.post.didLike.toggle()
        if post.didLike {
            // Unlike post here
            PostService.unlikePost(post: post) { _ in
                cell.likeButton.setImage(#imageLiteral(resourceName: "like_unselected"), for: .normal)
                cell.likeButton.tintColor = .black
                cell.viewModel?.post.likes = post.likes - 1
            }
            
        } else {
            // like here
            PostService.likePost(post: post) { _ in
                cell.likeButton.setImage(#imageLiteral(resourceName: "like_selected"), for: .normal)
                cell.likeButton.tintColor = .red
                cell.viewModel?.post.likes = post.likes + 1
                
                
                
                NotificationService.uploadNotification(toUid: post.ownerUid, fromUser: user, type: .like, post: post)
            }
        }
    }
    
    func cell(_ cell: FeedCell, wantsToShowProfileFor uid: String) {
        UserService.fetchUser(withUid: uid) { user in
            let controller = ProfileController(user: user)
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
