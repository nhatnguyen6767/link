//
//  UserCellViewModel.swift
//  Link
//
//  Created by none on 10/1/21.
//

import Foundation

/// populating user - cell - with this user object display correct data
/// populate user with this view model and use the view model to configure all of the user stuff

struct UserCellViewModel {
    private let user: User
    
    var profileImageUrl: URL? {
        return URL(string: user.profileImageUrl)
    }
    
    var username: String {
        return user.username
    }
    
    var fullname: String {
        return user.fullname
    }
    
    init(user: User) {
        self.user = user
    }
}
